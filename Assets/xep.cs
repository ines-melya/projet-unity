﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Complete
{
    public class xep : MonoBehaviour
    {
        public AudioSource Explosion;
        private GameObject Mine;
        public ParticleSystem ParticleSystem;
        public float m_MaxDamage = 15f;                    // The amount of damage done if the explosion is centred on a tank.
        public float m_ExplosionForce = 20f;              // The amount of force added to a tank at the centre of the explosion.
        public float m_MaxLifeTime = 2f;                    // The time in seconds before the shell is removed.
        public float m_ExplosionRadius = 5f;
        public LayerMask m_TankMask;                        // The maximum distance away from the explosion tanks can be and are still affected.


        // Start is called before the first frame update
        void Start()
        {
            ParticleSystem.Stop();

        }


        void OnTriggerEnter(Collider other)
        {
            Collider[] colliders = Physics.OverlapSphere(transform.position, m_ExplosionRadius, m_TankMask);

            // Calculate the amount of damage the target should take based on it's distance from the shell.

            for (int i = 0; i < colliders.Length; i++)
            {



                Debug.Log(colliders[i].gameObject.name);
                Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();

                if (!targetRigidbody) continue;

                targetRigidbody.AddExplosionForce(m_ExplosionForce, transform.position, m_ExplosionRadius);


                Debug.Log("wsh alors");
                TankHealth targetHealth = targetRigidbody.GetComponent<TankHealth>();
                if (!targetHealth) continue;
                Debug.Log(targetHealth.ToString());
                float damage = CalculateDamage(targetRigidbody.position);
                Debug.Log("coucou");
                targetHealth.TakeDamage(damage);

                // If they don't have a rigidbody, go on to the next collider.


                // Play the explosion sound effect.
                //GetComponent<AudioSource>().PlayOneShot(Explosion);

            }
            ParticleSystem.transform.parent = null;
            ParticleSystem.Play();
            Explosion.Play();
            Destroy(ParticleSystem.gameObject, ParticleSystem.duration);
            Destroy(gameObject);
        }

        public float CalculateDamage(Vector3 targetPosition)
        {
            // Create a vector from the shell to the target.
            Vector3 explosionToTarget = targetPosition - transform.position;

            // Calculate the distance from the shell to the target.
            float explosionDistance = explosionToTarget.magnitude;

            // Calculate the proportion of the maximum distance (the explosionRadius) the target is away.
            float relativeDistance = (m_ExplosionRadius - explosionDistance) / m_ExplosionRadius;

            // Calculate damage as this proportion of the maximum possible damage.
            float damage = relativeDistance * m_MaxDamage;

            // Make sure that the minimum damage is always 0.
            damage = Mathf.Max(0f, damage);

            return damage;
        }


    }
}