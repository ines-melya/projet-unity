﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Complete
{
    public class TankMineDropping : MonoBehaviour
    {
        
        public int m_PlayerNumber = 1;
        public GameObject m_Mines;
        public Transform m_DropTransform;
        public float m_DropForce = 1f;
        [HideInInspector] public GameObject m_TankInstance;
        public Inventory Inventory;
       // public GameManager GameManager;
        private string m_MineButton;
        private GameObject m_Mine;
        public int CountDroppedMines = 0;

        void Start()
        {
            m_MineButton = "Mines" + m_PlayerNumber;
        }

        void Update()
        {
            if (Input.GetButtonDown(m_MineButton))
            {
                DropMine();
               // IsMineDropped();
            }
            
        }

        public int DropMine()
        {
            if (m_TankInstance.GetComponent<Inventory>().ConsumeMine())
            {
                Instantiate(m_Mines, m_DropTransform.position, Quaternion.identity);
                CountDroppedMines = CountDroppedMines + 1;
                return CountDroppedMines;
            }
            else return CountDroppedMines;
        }
        /*public int IsMineDropped()
        {
            if (DropMine() == true)
            {
                CountDroppedMines=CountDroppedMines + 1;
                
            }
            return CountDroppedMines;
        }*/
    }
}