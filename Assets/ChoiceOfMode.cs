﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChoiceOfMode : MonoBehaviour
{
    public void LoadGame()
    {
        SceneManager.LoadScene(2);
    }
    public void LoadEasyGameSnow()
    {
        SceneManager.LoadScene(3);
    }
    public void LoadMediumGameSnow()
    {
        SceneManager.LoadScene(4);
    }
    public void LoadHardCoreGameSnow()
    {
        SceneManager.LoadScene(5);
    }
    public void LoadEasyGameChurch()
    {
        SceneManager.LoadScene(6);
    }
    public void LoadMediumGameChurch()
    {
        SceneManager.LoadScene(7);
    }
    public void LoadHardCoreGameChurch()
    {
        SceneManager.LoadScene(8);
    }
    public void QuitGame()
    {
        Application.Quit();
        Debug.Log("Quit!");
    }
}
