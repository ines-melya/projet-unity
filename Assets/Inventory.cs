﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour
{

    public int m_InitialMineStock = 3;
    public int m_MaxMineStock = 100;

   public Slider m_MineStockSlider;
    public Image m_FillImage;
    public Color m_FullMineStockColor;
    public Color m_EmptyMineStockColor;

    public int m_CurrentMineStock;
    private const int DEFAULT_INITIAL_MINE_STOCK = 5;
    private const int DEFAULT_MAX_MINE_STOCK = 100;

    private int NbMineInventaire;
    private int ActualNbMineInventaire;
    public int countconsumedmine = 0;
    public int NumberAmmo =0;
    public int NumberOfHeartsCollected = 0;
    public int CountCollideWithMine = 0;
    private void OnEnable()
    {
        m_CurrentMineStock = m_InitialMineStock;
        NbMineInventaire = m_InitialMineStock;
       ActualNbMineInventaire = m_InitialMineStock;
    m_MineStockSlider.maxValue = m_MaxMineStock;
       SetMineInventoryUI();
    }
    public bool ConsumeMine()
    {
        if (m_CurrentMineStock > 0)
        {
            m_CurrentMineStock--;
            ActualNbMineInventaire--;
            SetMineInventoryUI();
            return true;
           
        }
        else return false;
    }
    public int CountConsumedMine()
    {
        if (ConsumeMine() == true)
        {
            countconsumedmine += 1;
        }
        return countconsumedmine;
    }
    public int  OnTriggerEnter(Collider other)
    {
        if (other.tag == "Reload")
        {
            m_CurrentMineStock = m_CurrentMineStock + 3;
            NbMineInventaire += 3;
            ActualNbMineInventaire += 3;
            NumberAmmo += 1;
            return NumberAmmo;
        }
        if (other.tag == "Life")
        {
           
            NumberOfHeartsCollected += 1;
            return NumberOfHeartsCollected;
        }
        if (other.tag == "Mine")
        {

            CountCollideWithMine += 1;
            return CountCollideWithMine;
        }
        return 0;
    }
    public int GetActualNbMineInventaire()
    {
        return ActualNbMineInventaire;
    }
    public int GetStartNbMineInventaire()
    {
        return NbMineInventaire;
    }
    public void SetMineInventoryUI()
    {
        m_MineStockSlider.value = m_CurrentMineStock;
        m_FillImage.color = Color.Lerp(m_EmptyMineStockColor, m_FullMineStockColor, (float)m_CurrentMineStock / (float)m_MaxMineStock);
      
    }

    public bool ConfigureMineInitialStock(int nbMines)
    {
        if (nbMines >= 0 && nbMines <= m_MaxMineStock)
        {
            m_InitialMineStock = nbMines;
            return true;
        }
        else return false;
    }
    public bool ConfigureMineMaxStock(int nbMines)
    {
        if (nbMines >= 0 && nbMines >= m_InitialMineStock)
        {
            m_MaxMineStock = nbMines;
            return true;
        }
        else return false;
    }
    public void ResetConfiguration()
    {
        m_InitialMineStock = DEFAULT_INITIAL_MINE_STOCK;
        m_MaxMineStock = DEFAULT_MAX_MINE_STOCK;
    }
    // Start is called before the first frame update
   
}
