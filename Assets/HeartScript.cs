﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeartScript : MonoBehaviour
{

    public int NumberOfHeartsCollected = 0;
    public int  OnTriggerEnter(Collider other)
    {

        NumberOfHeartsCollected += 1;
        Destroy(gameObject);
        return NumberOfHeartsCollected;
    }
}

