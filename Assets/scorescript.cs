﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Complete
{
    public class scorescript : MonoBehaviour
    {
        public Text ActualNbMinesOne;
        public Text ActualNbMinesTwo;

        public Text NbMinesOne;
        public Text NbMinesTwo;

        private static Text NbMines;

        public static Inventory inventoryOne;
        public static Inventory inventoryTwo;

        public static TankMineDropping tankMineDroppingOne;
        public static TankMineDropping tankMineDroppingTwo;
        public Text DroppedMinesTwo;
        public Text DroppedMinesOne;
        //public TankMineDropping tankMineDropping;
        public Text CollisionWithMinesOne;
        public Text CollisionWithMinesTwo;

       
        public Text ShotFiredOne;
        public Text ShotFiredTwo;

        public static TankShooting tankShootingOne;
        public static TankShooting tankShootingTwo;

        public static TankHealth tankHealthOne;
        public static TankHealth tankHealthTwo;
        //public Text ShotDamageOne;
        //public Text ShotDamageTwo;

        //public static TankHealth tankHealthOne;
        //public static TankHealth tankHealthTwo;
        public Text HeartCollectedOne;
        public Text HeartCollectedTwo;
        public TankHealth TankHealth;
        public Inventory inventory;
        public Text AmmoOne;
        public Text AmmoTwo;
        private Collider other;

        public Text CollideMineOne;
        public Text CollideMineTwo;
        //public int m_PlayerNumber = 1;

        // Start is called before the first frame update
        void Start()
        {

            GetStartNbMineInventaire();
            GetActualNbMineInventaire();
            GetNumberOfMineDropped();
            
            HeartsCollected();
            AmmoCollected();
            CollideWithMines();
            ShotFired();
        }
        public void CollideWithMines()
        {
            CollideMineOne.text = inventoryOne.CountCollideWithMine.ToString();
            CollideMineTwo.text = inventoryTwo.CountCollideWithMine.ToString();
        }
        public void AmmoCollected()
        {

            AmmoOne.text = inventoryOne.NumberAmmo.ToString();
            AmmoTwo.text = inventoryTwo.NumberAmmo.ToString().ToString();
        }
        public void HeartsCollected()
        {
            HeartCollectedOne.text = inventoryOne.NumberOfHeartsCollected.ToString();
            HeartCollectedTwo.text = inventoryTwo.NumberOfHeartsCollected.ToString();
        }
        public void GetNumberOfMineDropped()
        {
           //DroppedMinesOne.text = inventoryOne.countconsumedmine.ToString();
            DroppedMinesOne.text = tankMineDroppingOne.CountDroppedMines.ToString();
            DroppedMinesTwo.text = tankMineDroppingTwo.CountDroppedMines.ToString();
        }
       /* public void ShotDamage()
        {
            ShotDamageOne.text = tankHealthOne.GreatShot.ToString();
            ShotDamageTwo.text = tankHealthTwo.GreatShot.ToString();

        }*/
         
    public void ShotFired()
        {
            ShotFiredOne.text = tankShootingOne.shoot.ToString();
            ShotFiredTwo.text = tankShootingTwo.shot_fired.ToString();
        }
        
        public void GetActualNbMineInventaire()

        {
 
                ActualNbMinesOne.text = inventoryOne.m_CurrentMineStock.ToString();
            ActualNbMinesTwo.text = inventoryTwo.m_CurrentMineStock.ToString();
        }
        public void GetStartNbMineInventaire()

        {

            //NbMines = transform.Find("NumberDecMines").GetComponent<Text>();
            NbMinesOne.text = inventoryOne.GetStartNbMineInventaire().ToString();
            NbMinesTwo.text = inventoryTwo.GetStartNbMineInventaire().ToString();

        }
        public static void AddDroppedMines(TankMineDropping tankMineDropping)
        {
            if (tankMineDroppingOne == null)
            {
                tankMineDroppingOne = tankMineDropping;
            }
            else if (tankMineDroppingTwo == null)
            {
                tankMineDroppingTwo = tankMineDropping;
            }
        }
        public static void AddDamage(TankHealth tankHealth)
        {
        
            if (tankHealthOne == null)
            {
                tankHealthOne = tankHealth;
            }
            else if (tankHealthTwo == null)
            {
                tankHealthTwo = tankHealth;
            }

        }
        public static void AddHearts(TankHealth tankHealth)
        {

            if (tankHealthOne == null)
            {
                tankHealthOne = tankHealth;
            }
            else if (tankHealthTwo == null)
            {
                tankHealthTwo = tankHealth;
            }

        }
        public static void AddShoot(TankShooting TankShooting)
        {

       
            if (tankShootingOne == null)
            {
                tankShootingOne = TankShooting;
            }
            else if (tankShootingTwo == null)
            {
                tankShootingTwo = TankShooting;
            }
        }
        public static void AddInventory(Inventory inventory)
        {
            if (inventoryOne == null)
            {
                inventoryOne = inventory;
            }
            else if (inventoryTwo== null)
            {
                inventoryTwo = inventory;
            }
        }
        // Update is called once per frame
        void Update()
        {
            GetStartNbMineInventaire();
            GetActualNbMineInventaire();
            GetNumberOfMineDropped();
           
            HeartsCollected();
            AmmoCollected();
            CollideWithMines();
            ShotFired();
        }
    }
}