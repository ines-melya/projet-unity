﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicPlayerScript : MonoBehaviour
{
    public Slider Slider;
    
    public AudioSource AudioSource;
    private float MusicVolume = 1f;


    // Start is called before the first frame update
    private void Start()
    {
        
        AudioSource.Play();
        MusicVolume = PlayerPrefs.GetFloat("volume");
        AudioSource.volume = MusicVolume;
        Slider.value = MusicVolume;
       
    }

    // Update is called once per frame
    private void Update()
    {
        AudioSource.volume = MusicVolume;
        PlayerPrefs.SetFloat("volume", MusicVolume);

    }

    public void VolumeUpdater(float volume)
    {
        MusicVolume = volume;
    }
    public void MusicReset()
    {
        PlayerPrefs.DeleteKey("volume");
        AudioSource.volume = 1;
        Slider.value = 1;
    }
}
