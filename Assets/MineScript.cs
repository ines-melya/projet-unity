﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Complete
{
    public class MineScript : MonoBehaviour
    {
        public AudioClip Explosionclip; 
        public AudioSource Explosionsource;
        private GameObject Mine;
        public ParticleSystem ParticleSystem;
        public float m_MaxDamage = 1000f;                    // The amount of damage done if the explosion is centred on a tank.
        public float m_ExplosionForce = 1000f;              // The amount of force added to a tank at the centre of the explosion.
        public float m_MaxLifeTime = 10f;                    // The time in seconds before the shell is removed.
        public float m_ExplosionRadius = 5f;
        public LayerMask m_TankMask;
        //[HideInInspector] public GameObject m_Instance;   // The maximum distance away from the explosion tanks can be and are still affected.
        //public Transform m_SpawnPoint;
        //public Vector3 position;
        // Start is called before the first frame update
        void Start()
        {
            ParticleSystem.Stop();
            StartCoroutine(ExampleCoroutine());
        }

        // Used at the start of each round to put the tank into it's default state.
        /*public void Reset()
        {
            m_Instance.transform.position = m_SpawnPoint.position;

           position = new Vector3(Random.Range(-50.0f, 50.0f), 0, Random.Range(-50.0f, 50.0f));
            m_Instance.SetActive(false);
            m_Instance.SetActive(true);
        }*/
        IEnumerator ExampleCoroutine()
        {
            
            //yield on a new YieldInstruction that waits for 5 seconds.
            yield return new WaitForSeconds(50);

        }
        void OnTriggerEnter(Collider other)
        {
            Collider[] colliders = Physics.OverlapSphere(transform.position, m_ExplosionRadius, m_TankMask);

            // Calculate the amount of damage the target should take based on it's distance from the shell.

            for (int i = 0; i < colliders.Length; i++)
            {



                Debug.Log(colliders[i].gameObject.name);
                Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();

                if (!targetRigidbody) continue;
                
                targetRigidbody.AddExplosionForce(m_ExplosionForce, transform.position, m_ExplosionRadius);


                
                TankHealth targetHealth = targetRigidbody.GetComponent<TankHealth>();
                if (!targetHealth) continue;
                
                float damage = CalculateDamage(targetRigidbody.position);
                
                targetHealth.TakeDamage(damage);

               
                Explosionsource.clip = Explosionclip;
                Explosionsource.Play();
                //GetComponent<AudioSource>().PlayOneShot(Explosion);

                }
            StartCoroutine(ExampleCoroutine());
            ParticleSystem.transform.parent = null;
                    ParticleSystem.Play();
                    Explosionsource.Play();
                    Destroy(ParticleSystem.gameObject, ParticleSystem.duration);
                    Destroy(gameObject);
            }

                    public float CalculateDamage(Vector3 targetPosition)
                    {
                            // Create a vector from the shell to the target.
                            Vector3 explosionToTarget = targetPosition - transform.position;

                            // Calculate the distance from the shell to the target.
                            float explosionDistance = explosionToTarget.magnitude;

                            // Calculate the proportion of the maximum distance (the explosionRadius) the target is away.
                            float relativeDistance = (m_ExplosionRadius - explosionDistance) / m_ExplosionRadius;

                            // Calculate damage as this proportion of the maximum possible damage.
                            float damage = relativeDistance * m_MaxDamage;

                            // Make sure that the minimum damage is always 0.
                            damage = Mathf.Max(0f, damage);

                            return damage;
                    }


    }
}

